#+TITLE: ANLP 2017/18 final project planning paper
#+AUTHOR: Kuan Yu \\ kuanyu@uni-potsdam.de
#+DATE: \today
#+OPTIONS: title:t date:t num:nil
#+OPTIONS: toc:nil author:t email:nil
#+LATEX_HEADER: \usepackage{enumitem}
#+LATEX_HEADER: \setlist{noitemsep}
#+LATEX_HEADER: \usepackage{xcolor}
#+LATEX_HEADER: \definecolor{darkblue}{rgb}{0,0,0.5}
#+LATEX_HEADER: \hypersetup{colorlinks=true,allcolors=darkblue}
#+LATEX_HEADER: \usepackage[sorting=ynt,style=authoryear,uniquename=false]{biblatex}
#+LATEX_HEADER: \addbibresource{planning.bib}

- Jacob Löbkens
- Jonas Mikkelsen
- Kuan Yu
- Miroslav Vitkov
- Till Ilić

* Task

Our project is part-of-speech tagging on tweets,
with guidelines and annotated data provided by \textcite{gimpel2013annotation}.[fn:1]
We will use the twpos-data-v0.3 corpus.[fn:2]
It is split into train-, dev-, and test-sets.

** Baseline model

We will adapt the hidden Markov tagger we implemented for the second assignment as the baseline.
This subtask mainly involves optimizing the smoothing method.

** Conditional random field (CRF) model

Our main subtask is the implementation of a CRF tagger,
modeling after \textcite{gimpel2011part}.
Their scores were close to the annotator agreement,
so we do not expect to surpass their performance,
but only to achieve similar results.

** Recurrent neural network (RNN) model

We will also try a more fashionable machine learning model for comparison.
Details are discusses in the next section.

* My subtask

I will be responsible for developing the RNN model.
Currently, the design is a two-leveled bidirectional RNN.
The first level constructs a representation for individual tokens from character embedding.
The second level constructs a representation for tokens within the context of a tweet.

* Challenges

- Informal and messy language, full of rare characters
- Small train-set, with merely 1000 tweets and 14619 tokens
- Many long and rare tokens, such as URLs and hashtags

* Objectives

- Familiarize ourselves with the procedure of a typical NLP task
  + Data preparation and processing
  + Baseline setup
  + Model designing
  + Model testing and tuning
  + Model evaluation and comparison
- Experience programming in a group
  + Coordinate subtasks
  + Version control and repository management
  + Bug report and resolution
- Learn and experiment with new machine learning algorithms

\printbibliography[]

* Footnotes

[fn:1] http://www.cs.cmu.edu/~ark/TweetNLP/

[fn:2] https://code.google.com/archive/p/ark-tweet-nlp/downloads
