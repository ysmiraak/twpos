\documentclass[11pt,a4paper,twocolumn]{article}
\usepackage{fontspec}
\usepackage{xcolor}
\definecolor{darkblue}{rgb}{0,0,0.5}
\usepackage[colorlinks=true,allcolors=darkblue]{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{enumitem}
\setlist{noitemsep}
\usepackage[font=small]{caption}
\usepackage{graphicx}
\graphicspath{ {image/} }
\usepackage[sorting=ynt,style=authoryear,uniquename=false]{biblatex}
\addbibresource{yu-projectreport.bib}
\author{Kuan Yu \\ \normalsize\tt kuanyu@uni-potsdam.de}
\date{\today}
\title{Part-of-speech tagging for a small corpus of tweets with conditional random field and recurrent neural network}
\begin{document}
\maketitle

\begin{abstract}
  On a small corpus of English tweets, we trained two taggers,
  one using a conditional random field with handcrafted linguistic features
  and one using a character-based recurrent neural network,
  achieving respectively a testing accuracy of 89.40\% and 87.33\%.
\end{abstract}

\section{Introduction}
\label{sec:intro}

We experimented with part-of-speech tagging on a small corpus of English tweets,
primarily with a conditional random field (CRF) and a recurrent neural network (RNN),
as the final project for the course \emph{Advanced natural language processing 2017}.%
\footnote{\url{http://www.ling.uni-potsdam.de/~scheffler/teaching/2017advancednlp/index.html}\\
  Group 6: Jacob Löbkens, Jonas Mikkelsen, Kuan Yu, Miroslav Vitkov, Till Ilić}
This is my project report.

The CRF tagger with handcrafted linguistic features achieved 89.40\% testing accuracy,
while the RNN tagger with raw characters achieved 87.33\%.

I will first describe the data (Section~\ref{sec:dat}),
then the CRF model (Section~\ref{sec:crf}) and the RNN model (Section~\ref{sec:rnn}),
and finally the results and analysis (Section~\ref{sec:res}).
Since I was charged with the implementation of the RNN model,
it will be the most detailed section in this report.
For more details, please consult the reports of my colleagues,
available in the project repository,
along with our program source code,
under the MIT license.%
\footnote{\url{https://bitbucket.org/cogsys_anlp_twpos/twpos}}

\section{Twitter corpus}
\label{sec:dat}

We used the tokenized and POS-tagged data with guidelines provided by \textcite{gimpel2013annotation}.%
\footnote{\url{http://www.cs.cmu.edu/~ark/TweetNLP/}}
The tagset consists of 25 tags tailored for tweets.
The annotator agreement was 92.2\%.
Table~\ref{tab:ds} shows the size of the corpus.%
\footnote{\url{https://code.google.com/archive/p/ark-tweet-nlp/downloads}}
It is split into three datasets, for training, development, and testing.

\begin{table}
  \centering
  {\small\ttfamily
    \begin{tabular}{lcc}
      \toprule
      dataset &tweets &words\\
      \midrule
      train &1\,000 &14\,619\\
      dev   &~\,327 &~4\,823\\
      test  &~\,500 &~7\,152\\
      \bottomrule
    \end{tabular}}
  \caption[]{\label{tab:ds}\texttt{twpos-data-v0.3}}
\end{table}

The training set is rather small, with merely 5433 word types, 77\% of which are hapaxes.
A word in tweets can often be quite long, when it is an URL, a hashtag, or an at-mention.
Furthermore, 23.7\% of the 156 character types appear only once.
These aspects present interesting challenges for working with this corpus.

On the plus side, a tweet has limited length, 15 words on average.
Therefore sentence segmentation is not necessary.
We simply treated a tweet as a sentence.

We implemented a baseline tagger using a standard hidden Markov model (HMM) with Laplace smoothing.
The testing accuracy is 71.50\%.

\section{Conditional random field}
\label{sec:crf}

Similar to HMMs, (linear-chain) CRFs \parencite{lafferty2001conditional} are often used
for modeling probabilities of hidden output sequences given some visible input sequence.
In our case, the input sequences are words and the output sequences are POS-tags.
Unlike an HMM however, a CRF is a discriminative model.
It is not concerned with modeling the probabilities of the visible data, namely the words.
For that reason, a CRF tagger can accept arbitrary linguistic features as the input,
and not just the identity of the words.
Features for the current input can even be from any other time steps in the sequence.

\subsection*{Linguistic features}

We included the following features, mostly adapted from \textcite{gimpel2011part}.
On an earlier version of the same dataset, they reported 89.37\% testing accuracy.

\textbf{Categorical features}, which were transformations of the word form.
For example, the phonetic normalization \parencite{philips1990hanging} to cope with alternate spellings,
and the word shape which had the characters mapped to character classes (digit, lower, upper, or others).

\begin{itemize}
\item Lower-cased word form
\item Phonetic normalization
\item Word shape
\item Suffix up to length 3
\item Prefix up to length 3
\end{itemize}

\textbf{Binary features}, which were obtained by matching against regular expressions or word lists.%
\footnote{\url{https://github.com/dominictarr/random-name} \url{https://en.wikipedia.org/wiki/List_of_emoticons}}

\begin{itemize}
\item Is it capitalized?
\item Does it contain hyphens?
\item Does it contain possessive markers?
\item Does it contain numbers?
\item Does it match a hashtag?
\item Does it match an at-mention?
\item Does it match an URL\@?
\item Is it a name?
\item Is it a family name?
\item Is it a place?
\item Is it a emoticon?
\end{itemize}

These features were extracted from the current word and its immediate predecessor and successor.

For the current word,
also included were the \textbf{tag dictionary}, i.e.\ frequencies of coarse-grained PTB tags in the WSJ and the Brown corpora,
and \textbf{pretrained word vectors} from the Google News Corpus.%
\footnote{\url{https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing}}.

We also added the \textbf{relative position} of the current word in the tweet,
and the global \textbf{sentence type},
specifically whether the tweet ends with a question mark and whether it starts with ``wh''.

\section{Recurrent neural network}
\label{sec:rnn}

RNNs are also frequently used in sequence modeling.
Like CRFs, they are discriminative models.
However, thanks to their powerful non-linear hidden layers,
neural networks are capable of extracting useful higher-order features from the raw data by themselves.
Therefore no feature engineering is required.

The usual RNNs can learn to compute the output at any time step
with information from all previous time steps,
if it needs and has enough capacity to do so.
Our RNN model is bidirectional \parencite{schuster1997bidirectional},
which means that while one recurrent layer processes some sequence,
another recurrent layer processes the reversed sequence,
after which their results are concatenated.
This way, all the past and the future information can be accessed.

\subsection*{Structure}

A tweet is a sequence of words, and a word is a sequence of characters.
Our character-based RNN works along those two axes in separate stages.

Firstly, the characters are embedded in a vector space.
This is done through an embedding layer.
Characters with unique appearances are replaced with a special dummy character.
Secondly, two recurrent layers on the word level learn a word representation from a sequence of characters.
Up to this level, the network acts as an embedding function for words.
Thirdly, two recurrent layers on the tweet level map a sequence of word vectors to a sequence of context-dependent word vectors.
Finally, a softmax layer maps each context-dependent word vector to a probability distribution over tags.
The tag with the highest likelihood is the predicted one.

As described, our network had in total six layers.
We found this to be the optimal depth.
Between consecutive layers,
we applied 50\% dropout \parencite{srivastava2014dropout} during training to reduce overfitting.
The latent spaces were always 200 dimensional,
whether it was the space for characters or for words.

In all recurrent layers, we used gated recurrent units \parencite{chung2014empirical}.
GRUs are usually used with the \texttt{tanh} activation function.
However we found that \texttt{relu} activation worked equally well,
while being computationally less expensive.

All parameters were initialized in an orthogonal scheme \parencite{saxe2013exact},
and updated through backpropagation using the Adam algorithm \parencite{kingma2014adam}
in small batches of size 16.
Each batch of tweets were randomly resampled from the training set.
We found a small batch size and resampling to be effective against overfitting.
The network was trained for \(2^{17}\) updates, and saved once every \(2^{9}\) updates.
We chose the saved model with the highest development accuracy,
which was the one right after the \(73,728^{th}\) update.

In our implementation, words longer than 32 characters were right-trimmed.
Even though RNNs are capable of dynamically handling sequences of arbitrary lengths,
we did it for efficiency.
In fact, longer words, mostly URLs, are only superfluously long for the task of tagging.
This was arguably the only linguistic insight in this model.

\subsection*{Why character-based?}

Since the first half of our character-based RNN model acts as an embedding function for words,
why not directly include the word identity as a feature,
by adding an embedding layer for words,
rather than solely rely on constructing the word representation from characters?

\begin{figure}
  \centering
  \includegraphics[width=0.48\textwidth]{acc.png}
  \caption[]{\label{fig:acc}Development accuracies of RNN taggers during the first 8,000 updates.
    One with direct word embedding (orange) and one without (blue).}
\end{figure}

Figure~\ref{fig:acc} answers the question.
When the word feature was included directly,
the model learned much quicker in the beginning,
but also overfitted quickly.
The purely character-based model slowly caught up,
and kept progressing.
Even though both models had raw characters as feature
and could theoretically learn the same function since the characters in a word is exactly its identity,
the model which directly made use of the word identity never recovered from overfitting.

\section{Results and conclusion}
\label{sec:res}

\begin{table}
  \centering
  {\small
    \begin{tabular}{cccl}
      \toprule
      tag &CRF &RNN &main description\\
      \midrule
      \texttt{V} &90.14 &\textbf{91.94} &verb\\
      \texttt{N} &79.12 &\textbf{84.37} &common noun\\
      \texttt{,} &\textbf{97.68} &96.47 &punctuation\\
      \texttt{P} &93.41 &\textbf{93.76} &preposition\\
      \texttt{O} &95.44 &\textbf{96.30} &pronoun\\
      \texttt{\textasciicircum} &63.51 &\textbf{74.11} &proper noun\\
      \texttt{D} &93.58 &\textbf{94.99} &determiner\\
      \texttt{A} &68.73 &\textbf{79.13} &adjective\\
      \texttt{@} &99.70 &\textbf{99.85} &at-mention\\
      \texttt{R} &82.44 &\textbf{84.52} &adverb\\
      \texttt{\textasciitilde} &\textbf{96.04} &90.74 &discourse marker\\
      \texttt{!} &\textbf{85.87} &83.11 &interjection\\
      \texttt{L} &\textbf{93.75} &93.65 &nominal + verbal\\
      \texttt{\&} &\textbf{98.43} &95.55 &coordinating conjunction\\
      \texttt{U} &\textbf{99.57} &98.70 &URL\\
      \texttt{\$} &88.40 &\textbf{94.12} &numeral\\
      \texttt{E} &\textbf{84.62} &82.64 &emoticon\\
      \texttt{\#} &85.38 &\textbf{89.53} &hashtag\\
      \texttt{G} &32.99 &\textbf{33.63} &others\\
      \texttt{T} &\textbf{71.43} &68.66 &verb particle\\
      \texttt{Z} &\textbf{70.00} &65.00 &proper noun + possessive\\
      \texttt{S} &40.00 &\textbf{54.55} &nominal + possesive\\
      \texttt{X} &36.36 &\textbf{61.54} &existential\\
      \midrule
      &\textbf{89.40} &87.33\\
      \bottomrule
    \end{tabular}}
  \caption[]{\label{tab:res}Testing results,
    including the per tag \(F_{1}\) scores,
    ordered by frequencies of tags in the training set,
    and the overall accuracies.}
\end{table}

Table~\ref{tab:res} compares the testing results of our CRF and RNN taggers.
The former performed better overall,
thanks to the efforts invested in its feature selections.
Admittedly, the CRF model made use of data from multiple external sources,
including three other corpora and a few word lists,
while the RNN model saw only the training set.

Despite all the mechanisms against overfitting in our RNN model,
it made only one error on the training data.
Considering the relatively low degree of annotator agreement,
the CRF model had a much more reasonable training error rate of 4.6\%,
With a small training set,
overfitting is particularly easy and detrimental,
especially for high-capacity statistical machine learning models.

We also explored methods for an ensemble model,
seeking to combine the strengths of our existing taggers.
However, they were not successful,
mainly because we had too few adequate models.

Thus concludes my project report.%
\footnote{Word count: 1985}

\printbibliography[]

\end{document}
