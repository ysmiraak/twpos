from collections import Counter
import numpy as np
import os


def path(part, root= "../twpos-data-v0.3/oct27.splits"):
    """-> str"""
    return os.path.join(root, "oct27.{}".format(part))


def load(path):
    """-> pair (tuple (tuple str)) : tokens, tags"""
    with open(path, encoding= 'utf-8') as file:
        toks, tags, tok_tag = [], [], []
        for line in file:
            line = line.rstrip("\n")
            if not line:
                if tok_tag:
                    tok, tag = zip(*tok_tag)
                    toks.append(tok)
                    tags.append(tag)
                    tok_tag = []
            else:
                tok_tag.append(line.split("\t"))
                assert 2 == len(tok_tag[-1])
        if tok_tag:
            tok, tag = zip(tok_tag)
            toks.append(tok)
            tags.append(tag)
        return tuple(toks), tuple(tags)


def rank(xs, min_freq= 2):
    """ranks items in `xs` by frequency."""
    return tuple((x for x, f in Counter(xs).most_common() if min_freq <= f))


class Index:
    """takes a vector of `m` unique items, returns a bijective `index`,
    such that:

    `index[i]` returns the item at index `i` but only if `0 <= i < m`;

    `index(x)` returns the index for item `x`, or `m` for unknown items.

    """

    def __init__(self, vec):
        self._i2x = tuple(vec)
        self._x2i = {x: i for i, x in enumerate(self._i2x)}

    def __len__(self):
        return len(self._i2x)

    def __getitem__(self, i):
        assert 0 <= i < len(self._i2x)
        return self._i2x[i]

    def __call__(self, x):
        try:
            return self._x2i[x]
        except KeyError:
            return len(self._x2i)


def jagged_array(x, fill, shape, dtype):
    """-> np.ndarray; with jagged `x` trimmed and filled into `shape`."""
    a = np.full(shape= shape, dtype= dtype, fill_value= fill)
    i, *shape = shape
    x = np.stack([jagged_array(x= x, fill= fill, shape= shape, dtype= dtype) for x in x]) \
                                  if shape else np.fromiter(x, dtype= dtype)
    i = min(i, len(x))
    a[:i] = x[:i]
    return a


def encode_x(text, index, max_chars= 32, dtype= np.uint8):
    """encodes the characters.

    text      : seq (seq str)
    index     : Index
    max_chars : int | None

    """
    return jagged_array(
        (((index(char) for char in word) for word in txt) for txt in text)
        , fill= len(index)
        , shape= (len(text)
                  , max(map(len, text))
                  , max_chars or max(len(word) for txt in text for word in txt))
        , dtype= dtype)


def encode_y(tags, index, dtype= np.uint8):
    """encodes the tags.

    text  : seq (seq str)
    index : Index

    """
    return jagged_array(
        ((index(tag) for tag in tags) for tags in tags)
        , fill= len(index)
        , shape= (len(tags), max(map(len, tags)))
        , dtype= dtype)


def encode_s(text, max_chars= 32, dtype= np.uint8):
    """encodes the length of words.

    text      : seq (seq str)
    max_chars : int | None

    """
    trim = (lambda n: min(n, max_chars)) if max_chars else (lambda n: n)
    return jagged_array(
        ((trim(len(word)) for word in txt) for txt in text)
        , fill= 0
        , shape= (len(text), max(map(len, text)))
        , dtype= dtype)
