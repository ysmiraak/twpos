from sklearn import metrics
import pandas as pd


pd.options.display.float_format = "{:.2f}".format


def print_metrics(gold, pred):
    labels = sorted({*gold, *pred})
    print("\naccuracy", metrics.accuracy_score(gold, pred))
    print("\n", pd.DataFrame(
        data= {
            'f1': 100 * metrics.f1_score(gold, pred, labels= labels, average= None)
            , 'prec': 100 * metrics.precision_score(gold, pred, labels= labels, average= None)
            , 'recall': 100 * metrics.recall_score(gold, pred, labels= labels, average= None)}
        , index= labels))
