from model import Model
from record import Record
import numpy as np
import tensorflow as tf


step_up = 2**16
step_ev = 2**9
from param import param


dat = Record(**np.load('dat_train.npy').item())
m = Model(trainable= True, dropout= 0.5, x= dat.x, y= dat.y, s= dat.s, **param)
del dat


feed_up = {m.bs_: 16, m.training: True}


dat = Record(**np.load('dat_dev.npy').item())
feed_ev = {m.x: np.array(dat.x.T), m.y: np.array(dat.y.T), m.s: np.array(dat.s.T)}
del dat


sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())


for _ in range(step_up):
    m.step += 1
    sess.run(m.up, feed_dict= feed_up)


saver = tf.train.Saver(max_to_keep= None)
summ_ev = tf.summary.scalar(name= 'step_acc', tensor= m.acc)
with tf.summary.FileWriter('log') as wtr:
    for _ in range(step_up):
        m.step += 1
        sess.run(m.up, feed_dict= feed_up)
        if not (m.step % step_ev):
            saver.save(sess, save_path= 'model/m', global_step= m.step)
            wtr.add_summary(sess.run(summ_ev, feed_dict= feed_ev), m.step)


sess.close()
