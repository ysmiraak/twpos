from record import Record
import tensorflow as tf

tf.set_random_seed(26)


class Model(Record):

    @staticmethod
    def old(**param):
        return Model(trainable= False, dropout= 0, x= None, y= None, s= None, **param)

    def __init__(self
                 , trainable
                 , x, y, s
                 , dim_x, dim_y
                 , dim_char, dim_word, dim_text, dim_dense
                 , masked= False
                 , dropout= 0.5):
        if trainable:
            with tf.name_scope('global/'):
                init_orth = tf.orthogonal_initializer()
                init_zero = tf.zeros_initializer()
                self.training = tf.placeholder_with_default(name='training', input= False, shape= ())
                self.dropout = tf.placeholder_with_default(name='dropout', input= dropout, shape= ())
                self.bs_ = tf.placeholder(name= 'bs_', dtype= tf.int32, shape= ())
                self.bat = tf.random_uniform(
                    name= 'bat', shape= (self.bs_,), minval= 0, maxval= len(s), dtype= tf.int32)
            with tf.name_scope('x'):
                self.x = tf.placeholder_with_default(
                    shape= (None, None, None)
                    , input= tf.cast(
                        dtype= tf.int32
                        , x= tf.gather(
                            params= tf.constant(x.T)
                            , indices= self.bat
                            , axis= -1)))
            with tf.name_scope('y'):
                self.y = tf.placeholder_with_default(
                    shape= (None, None)
                    , input= tf.cast(
                        dtype= tf.int32
                        , x= tf.gather(
                            params= tf.constant(y.T)
                            , indices= self.bat
                            , axis= -1)))
            with tf.name_scope('s'):
                self.s = tf.placeholder_with_default(
                    shape= (None, None)
                    , input= tf.cast(
                        dtype= tf.int32
                        , x= tf.gather(
                            params= tf.constant(s.T)
                            , indices= self.bat
                            , axis= -1)))
        else:
            init_orth, init_zero = None, None
            with tf.name_scope('x'): self.x = tf.placeholder(tf.int32, shape= (None, None, None))
            with tf.name_scope('y'): self.y = tf.placeholder(tf.int32, shape= (None, None))
            with tf.name_scope('s'): self.s = tf.placeholder(tf.int32, shape= (None, None))
        with tf.name_scope('global/'):
            with tf.name_scope('shape'): shape = tf.shape(self.x)
            with tf.name_scope('max_tok'): max_tok = shape[0]
            with tf.name_scope('max_txt'): max_txt = shape[1]
            with tf.name_scope('len_bat'): len_bat = shape[2]
            with tf.name_scope('txt_bat'): txt_bat = max_txt * len_bat
            with tf.name_scope('len_tok'): len_tok = tf.reshape(self.s, shape= (txt_bat,))
            with tf.name_scope('len_txt'):
                self.len_txt = tf.reduce_sum(tf.cast(tf.not_equal(self.s, 0), dtype= tf.int32), axis= 0)
        self.embed = tf.get_variable(
            name= 'embed'
            , shape= (dim_x, dim_char)
            , dtype= tf.float32
            , initializer= init_orth)
        with tf.name_scope('char'):
            h = self.char = tf.gather(
                params= self.embed
                , indices= tf.reshape(self.x, shape= (max_tok, txt_bat))
                , axis= 0)
        for name, (fw, bw) in enumerate(dim_word, 1):
            if dropout: h = tf.layers.dropout(h, rate= self.dropout, training= self.training)
            name = 'word{}'.format(name)
            with tf.name_scope(name):
                h, h1 = tf.nn.bidirectional_dynamic_rnn(
                    scope= name
                    , cell_fw= tf.nn.rnn_cell.GRUCell(
                        num_units= fw
                        , activation= tf.nn.relu
                        , kernel_initializer= init_orth
                        , bias_initializer= init_zero)
                    , cell_bw= tf.nn.rnn_cell.GRUCell(
                        num_units= bw
                        , activation= tf.nn.relu
                        , kernel_initializer= init_orth
                        , bias_initializer= init_zero)
                    , inputs= h
                    , sequence_length= len_tok
                    , time_major= True
                    # , parallel_iterations= 1
                    # , swap_memory= True
                    , dtype= tf.float32)
                h = tf.concat(h, axis= -1)
                setattr(self, name, h)
        with tf.name_scope(name + '/'):
            h = tf.reshape(tf.concat(h1, axis= -1), shape= (max_txt, len_bat, fw + bw))
            setattr(self, name, h)
        for name, (fw, bw) in enumerate(dim_text, 1):
            if dropout: h = tf.layers.dropout(h, rate= self.dropout, training= self.training)
            name = 'text{}'.format(name)
            with tf.name_scope(name):
                h = tf.concat(
                    axis= -1
                    , values= tf.nn.bidirectional_dynamic_rnn(
                        scope= name
                        , cell_fw= tf.nn.rnn_cell.GRUCell(
                            num_units= fw
                            , activation= tf.nn.relu
                            , kernel_initializer= init_orth
                            , bias_initializer= init_zero)
                        , cell_bw= tf.nn.rnn_cell.GRUCell(
                            num_units= bw
                            , activation= tf.nn.relu
                            , kernel_initializer= init_orth
                            , bias_initializer= init_zero)
                        , inputs= h
                        , sequence_length= self.len_txt
                        , time_major= True
                        # , parallel_iterations= 1
                        # , swap_memory= True
                        , dtype= tf.float32)[0])
                setattr(self, name, h)
        with tf.name_scope(name + '/'):
            h = tf.reshape(h, shape= (txt_bat, fw + bw))
        for name, units in enumerate(dim_dense, 1):
            if dropout: h = tf.layers.dropout(h, rate= self.dropout, training= self.training)
            name = "dense{}".format(name)
            h = tf.layers.dense(
                name= name
                , inputs= h
                , units= units
                , activation= tf.nn.relu
                , kernel_initializer= init_orth
                , bias_initializer= init_zero)
            setattr(self, name, h)
        if dropout: h = tf.layers.dropout(h, rate= self.dropout, training= self.training)
        h = self.dense = tf.layers.dense(
            name= 'dense'
            , inputs= h
            , units= dim_y - 1 if masked else dim_y
            , kernel_initializer= init_orth
            , bias_initializer= init_zero)
        with tf.name_scope('pred'):
            self.pred = tf.reshape(
                shape= (max_txt, len_bat)
                , tensor= tf.argmax(
                    h if masked else h[:,:-1]
                    , axis= -1
                    , output_type=tf.int32))
        with tf.name_scope('mask'):
            self.mask = tf.transpose(tf.sequence_mask(lengths= self.len_txt, maxlen= max_txt))
        with tf.name_scope('acc'):
            self.acc = tf.reduce_sum(
                tf.cast(self.mask, dtype= tf.int32)
                * tf.cast(tf.equal(self.y, self.pred), dtype= tf.int32)
            ) / tf.reduce_sum(self.len_txt)
        if trainable:
            with tf.name_scope('loss'):
                if masked:
                    logits = tf.boolean_mask(h, tf.reshape(self.mask, shape= (txt_bat,)))
                    labels = tf.boolean_mask(self.y, mask= self.mask)
                else:
                    logits = h
                    labels = tf.reshape(self.y, shape= (txt_bat,))
                self.loss = tf.reduce_mean(
                    tf.nn.sparse_softmax_cross_entropy_with_logits(logits= logits, labels= labels))
            self.up = tf.train.AdamOptimizer().minimize(self.loss)
            self.step = 0
