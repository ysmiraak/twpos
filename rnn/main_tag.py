from main_param import param, x, y
from metric import print_metrics
from model import Model
from utils import path, load, Index, encode_x, encode_s
import tensorflow as tf


def tag(model, chars, tags, text, max_chars= 32):
    """-> tuple (tuple str) : tags

    model     : Model
    chars     : vec str
    tags      : vec str
    text      : vec (vec str)
    max_chars : int

    """
    assert 1 + len(chars) == model.embed.shape[0] < 256
    assert len(tags) <= model.dense.shape[1] <= 1 + len(tags) < 256
    x = Index(chars)
    y = Index(tags)
    return [[y[p] for p in pred[:len(t)]] for t, pred in zip(text, model.pred.eval(
        {model.x: encode_x(text, x, max_chars).T, model.s: encode_s(text, max_chars).T}
    ).T)]


m = Model.old(**param)
sess = tf.InteractiveSession()
tf.train.Saver().restore(sess= tf.get_default_session(), save_path= "final/model/73728")


test, gold = load(path('test'))
pred = tag(model= m, chars= x, tags= y, text= test)


flatten = lambda m: [c for r in m for c in r]
print_metrics(flatten(gold), flatten(pred))
