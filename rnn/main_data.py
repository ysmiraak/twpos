from utils import path, load, rank, Index, encode_x, encode_s, encode_y


toks, tags = load(path('train'))

x = Index(rank(char for row in toks for tok in row for c in tok[:32]))
y = Index(rank(tag  for row in tags for tag in row))

np.save("dat_train", {'x': encode_x(toks, x), 'y': encode_y(tags, y), 's': encode_s(toks)})


toks, tags = load(path('dev'))
np.save("dat_dev", {'x': encode_x(toks, x), 'y': encode_y(tags, y), 's': encode_s(toks)})
