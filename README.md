# rnn

- `final`: the final tensorflow model, tensorboard graph, and output
- `notes.org`: the experiment log and detailed final results
- `model.py`: the core implementation

main

- `main_tag.py`: tagging (the test-set) with the final saved model
- `main_param.py`: hyperparameters
- `main_data.py`: data preparation for training
- `main_train.py`: training

utilities

- `utils.py`
- `record.py`
- `metric.py`
